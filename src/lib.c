#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "hashset.h"
#include "linkedlist.h"
#include <time.h>
#include <unistd.h>
#define BILLION 1000000000.0

// Generate a prehash for an item with a given size
unsigned long prehash(void* item, unsigned int item_size) {
    unsigned long h = 5381;
    char *c = item;
    for (int i = 0; i < item_size; i++) { 
        h = ((h << 5) + h) + c[i];
    }
    return h;
}

// Hash an unsigned long into an index that fits into a hash set
unsigned long hash(unsigned long prehash, unsigned long buckets) {
    return prehash % buckets;
}

// Initialize an empty hash set with a given size
void initHashSet(HashSet* hashset_pointer, unsigned int size) {
    hashset_pointer->size = size;
    hashset_pointer->load_factor = 0;
    hashset_pointer->table = (List**) malloc(sizeof(List*) * size);
    for (int i = 0; i < size; i++) {
        List* list = (List *) malloc(sizeof(List));
        initList(list);
        hashset_pointer->table[i] = list;
    }

}

// Insert item in the set. Return true if the item was inserted, false if it wasn't (i.e. it was already in the set)
// Recalculate the load factor after each successful insert (round to nearest whole number).
// If the load factor exceeds 70 after insert, resize the table to hold twice the number of buckets.
bool insertItem(HashSet* hashset_pointer, void* item, unsigned int item_size) {
    unsigned long my_hash = hash(prehash(item, item_size), hashset_pointer->size);
    if (findItem(hashset_pointer, item, item_size)) {
        return false;
    }
    Node* node = malloc(sizeof(Node*));
    node = createNode(prehash(item, item_size));
    insertAtTail(hashset_pointer->table[my_hash], prehash(item, item_size));
    int filled_buckets = 0;
    for (int i = 0; i < hashset_pointer->size; i++) {
        if (hashset_pointer->table[i]->head != NULL) {
            filled_buckets++;
        }
    }
    filled_buckets *= 100;
    hashset_pointer->load_factor = filled_buckets / hashset_pointer->size;
    if (hashset_pointer->load_factor >= 70) {
        resizeTable(hashset_pointer, hashset_pointer->size * 2);
    }
    return true;
}

// Remove an item from the set. Return true if it was removed, false if it wasn't (i.e. it wasn't in the set to begin with)
bool removeItem(HashSet* hashset_pointer, void* item, unsigned int item_size) {
    unsigned long my_hash = hash(prehash(item, item_size), hashset_pointer->size);
    if (!findItem(hashset_pointer, item, item_size)) {
        return false;
    }
    Node* node;
    List* list = hashset_pointer->table[my_hash];
    if (list->head != NULL) {
        node = list->head;
        int index = 0;
        
        do {
            if (node->item == prehash(item, item_size)) {
                if (index == 0) {
                    removeHead(list);
                } else {
                    removeAtIndex(list, index);
                }
                return true;
            } else {
                index++;
                node = node->next;
            }
        } while (node != NULL);
    }
    return false;
}

// Return true if the item exists in the set, false otherwise
bool findItem(HashSet* hashset_pointer, void* item, unsigned int item_size) {
    unsigned long my_hash = hash(prehash(item, item_size), hashset_pointer->size);
    if (hashset_pointer->table[my_hash]->head != NULL) {
        Node* node = hashset_pointer->table[my_hash]->head;
        while (node != NULL) {
            if (node->item == prehash(item, item_size)) {
                return true;
            } else {
                node = node->next;
            }
        }
    }
    return false;
}

// Resize the underlying table to the given size. Recalculate the load factor after resize
void resizeTable(HashSet* hashset_pointer, unsigned int new_size) {
    HashSet* hp_temp;
    hp_temp = (HashSet*) malloc(sizeof(HashSet));
    initHashSet(hp_temp, new_size);
    for (int i = 0; i < hashset_pointer->size; i++) {
        Node* node;
        List* list = hashset_pointer->table[i];
        if (list->head != NULL) {
            node = list->head;
            do {
                Node* node2 = malloc(sizeof(Node*));
                node2 = createNode(node->item);
                insertAtTail(hp_temp->table[hash(node->item, hp_temp->size)], node->item);
                removeHead(list);
                node = list->head;
            } while (node != NULL);
        }
        free(list);
    }
    free(hashset_pointer->table);
    hashset_pointer->size = hp_temp->size;
    hashset_pointer->table = hp_temp->table;
    int filled_buckets = 0;
    for (int i = 0; i < hashset_pointer->size; i++) {
        if (hashset_pointer->table[i]->head != NULL) {
            filled_buckets++;
        }
    }
    filled_buckets *= 100;
    hashset_pointer->load_factor = filled_buckets / hashset_pointer->size;    
}

// Print Table
void printHashSet(HashSet* hashset_pointer) {
    for (int i = 0; i < hashset_pointer->size; i++) {
        List* list = hashset_pointer->table[i];
        printList(list);
    }
}

// #include <stdio.h>
// #include <stdlib.h>
// #include "linkedlist.h"

//Initialize List
void initList(List * lp) {
	lp->head = NULL;
	lp->tail = NULL;
}

//Create node and return reference of it.
Node * createNode(void* item) {
	Node * nNode;

	nNode = (Node *) malloc(sizeof(Node));

	nNode->item = item;
	nNode->next = NULL;

	return nNode;
}


//Add new item at the end of list.
void insertAtTail(List * lp, void* item) {
	Node * node;
	node = createNode((void*)item);

	//if list is empty.
	if(lp->head == NULL) {
		lp->head = node;
		lp->tail = node;
	} else {
		lp->tail->next  = node;
		lp->tail 	= lp->tail->next;
	}		
}


//Add new item at beginning of the list.
void insertAtHead(List * lp, void* item) {
	Node * node;
	node = createNode(item);

	//if list is empty.
	if(lp->head == NULL) {
		lp->head = node;
		lp->tail = node;
	} else {
		node->next 	= lp->head;
		lp->head 	= node;
	}		
}

//Add new item at beginning of the list.
void insertAtIndex(List * list, int index, void* item) {
	Node * to_insert;
	to_insert = createNode(item);

  int i = 0;
  Node* prev;
  Node* node = list->head;
  while (node != NULL) {
    if (i == index) {
      prev->next = to_insert;
      to_insert->next = node;
      return;
    } else if (i > index) {
      return;
    } else {
      i++;
      prev = node;
      node = node->next;
    }
  }	
}

void* removeAtIndex(List * list, int index) {
  int i = 0;
  Node* prev;
  Node* node = list->head;
  while (node != NULL) {
    if (i == index) {
      prev->next = node->next;
			void* item = node->item;
			free(node);
      return item;
    } else if (i > index) {
      // List is too short
			return NULL;
    } else {
      i++;
      prev = node;
      node = node->next;
    }
  }	
}

//Delete item from Start of list.
void* removeHead(List * lp) {
	void* item;

	if(lp->head == NULL) {	
		// List is Empty	
		return NULL;
	} else {
		item = lp->head->item;
		Node* old_head = lp->head;
		lp->head = lp->head->next;	
		free(old_head);	
	}	
	return item;
}

//Delete item from the end of list.
void* removeTail(List * lp) {
	Node * temp;
	int i = 0;

	void* item;

	if(lp->tail == NULL) {	
		// List is Empty	
		return NULL;
	}
	else {
		temp = lp->head;

		// Iterate to the end of the list
		while(temp->next != lp->tail) { 
			temp = temp->next;
		}

		item = lp->tail->item;

		Node* old_tail = lp->tail;
		lp->tail = temp;
		lp->tail->next = NULL;	
		free(old_tail);	
	}	
	return item;
}

void* itemAtIndex(List* list, int index) {
  int i = 0;
  Node* node = list->head;
  while (node != NULL) {
    if (i == index) {
      return node->item;
    } else if (i > index) {
      return NULL;
    } else {
      i++;
      node = node->next;
    }
  }

}

void printList(List* list) {
	Node* node;

  // Handle an empty node. Just print a message.
	if(list->head == NULL) {
		printf("\nEmpty List");
		return;
	}
	
  // Start with the head.
	node = (Node*) list->head;

	printf("\nList: \n\n\t"); 
	while(node != NULL) {
		printf("[ %x ]", node->item);

    // Move to the next node
		node = (Node*) node->next;

		if(node !=NULL) {
			printf("-->");
    }
	}
	printf("\n\n");
}

List* createList(){
  List* lp;
  void* item;
  lp = (List*)malloc(sizeof(List));
  initList(lp);
  return lp;
}

HashSet* createHashSet(int bucketSize){
  HashSet* hs;
  hs = (HashSet*)malloc(sizeof(HashSet));
  initHashSet(hs, bucketSize);
  return hs;
}


int calcTimeLL(FILE* file, List* lp){
    char buff[255];
    struct timespec start, end; 
    clock_gettime(CLOCK_REALTIME, &start);
    int n = 0;
    while (fgets(buff, 255, (FILE*)file) != NULL){
        insertAtHead(lp, file);                                               //insert URLs from file1 to linkedlist 1
        n++;
    }
    clock_gettime(CLOCK_REALTIME, &end);
    return (end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec) / n;
    // printf("Average time spent to insert file1 in linked list: " + ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/n);
}

int calcTimeHS(FILE* file, HashSet* hs){
    char buff[255];
    struct timespec start, end; 
    clock_gettime(CLOCK_REALTIME, &start);
    int n = 0;
    while (fgets(buff, 255, (FILE*)file) != NULL){
        insertItem(hs, file, sizeof(FILE));                                 //insert URLs from file1 to hs w/ 1e3 buckets  
        n++;
    }
    clock_gettime(CLOCK_REALTIME, &end);
    return (end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec) / n;
}

int calcFindTimeLL(FILE* file, List* lp){
  char buff[255];
  int index = -1;
  struct timespec start, end; 
  clock_gettime(CLOCK_REALTIME, &start);

  while (fgets(buff, 255, (FILE*)file) != NULL){
    index++;
    itemAtIndex(lp, index);
  }
  clock_gettime(CLOCK_REALTIME, &end);
  return (end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec) / index;
}

int calcFindTimeHS(FILE* file, HashSet* hs){
  char buff[255];
  int index = -1;
  struct timespec start, end; 
  clock_gettime(CLOCK_REALTIME, &start);

  while (fgets(buff, 255, (FILE*)file) != NULL){
    index++;
    findItem(hs, fgets(buff, 255, (FILE*)file), sizeof(fgets(buff, 255, (FILE*)file)));
  }
  clock_gettime(CLOCK_REALTIME, &end);
  return (end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec) / index;
}



