#include <time.h>
#include <unistd.h>
#define BILLION 1000000000.0

main(int argc, char **argc){
    FILE *file1 = argv[1];
    FILE *file2 = argv[2];
    FILE *file3 = argv[3];
    FILE *file4 = argc[4];
    
    //citation: Tutorialspoint C - File I/O
    char buff[255];
    // fgets(buff, 255, (FILE*)file1);
    
    //initilize linked list
    List* lp1 = createList();                                                   //linked list for file1
    List* lp2 = createList();                                                   //linked list for file2
    List* lp3 = createList();                                                   //linked list for file3
    List* lp4 = createList();                                                   //linked list for file4

    //initilize hashset
    int bucketSize = 1000;
    HastSet *hs3_1 = createHashSet(bucketSize);                                 //hashset(1e3) for file1  hsBucket#_File#
    HastSet *hs3_2 = createHashSet(bucketSize);                                 //hashset(1e3) for file2
    HastSet *hs3_3 = createHashSet(bucketSize);                                 //hashset(1e3) for file3
    HastSet *hs3_4 = createHashSet(bucketSize);                                 //hashset(1e3) for file4
    
    bucketSize = 10000
    HashSet *hs4_1 = createHashSet(bucketSize);                                 //hashset(1e4) for file 1
    HashSet *hs4_2 = createHashSet(bucketSize);                                 //hashset(1e4) for file 2
    HashSet *hs4_3 = createHashSet(bucketSize);                                 //hashset(1e4) for file 3
    HashSet *hs4_4 = createHashSet(bucketSize);                                 //hashset(1e4) for file 4
    
    bucketSize = 100000
    HashSet *hs5_1 = createHashSet(bucketSize);                                           //hashset(1e5) for file 1
    HashSet *hs5_2 = createHashSet(bucketSize);                                           //hashset(1e5) for file 2
    HashSet *hs5_3 = createHashSet(bucketSize);                                           //hashset(1e5) for file 3
    HashSet *hs5_4 = createHashSet(bucketSize);                                           //hashset(1e5) for file 4
    
    bucketSize = 1000000
    HashSet *hs6_1 = createHashSet(bucketSize);                                           //hashset(1e6) for file 1
    HashSet *hs6_2 = createHashSet(bucketSize);                                           //hashset(1e6) for file 2
    HashSet *hs6_3 = createHashSet(bucketSize);                                           //hashset(1e6) for file 3
    HashSet *hs6_4 = createHashSet(bucketSize);                                           //hashset(1e6) for file 4
    
    //insert file1 into linkedlist and hashsets and calculate insertion time
    printf("Average time spent to insert file1 in linked list: " + calcTimeLL(file1, lp1));
    //insert file1 into hs w/1e3,4,5,6 buckets and calculate insertion timee
    printf("Average time spent to insert file1 in HashSet w/1e3 buckets: " + calcTimeHS(file1, hs3_1));
    printf("Average time spent to insert file1 in HashSet w/1e4 buckets: " + calcTimeHS(file1, hs4_1));
    printf("Average time spent to insert file1 in HashSet w/1e5 buckets: " + calcTimeHS(file1, hs5_1));
    printf("Average time spent to insert file1 in HashSet w/1e6 buckets: " + calcTimeHS(file1, hs6_1));
    
    //insert file2 into linkedlist and hashsets and calculate insertion time
    printf("Average time spent to insert file2 in linked list: " + calcTimeLL(file2, lp2));
    //insert file2 into hs w/1e3,4,5,6 buckets and calculate insertion time
    printf("Average time spent to insert file2 in HashSet w/1e3 buckets: " + calcTimeHS(file2, hs3_2));
    printf("Average time spent to insert file2 in HashSet w/1e4 buckets: " + calcTimeHS(file2, hs4_2));
    printf("Average time spent to insert file2 in HashSet w/1e5 buckets: " + calcTimeHS(file2, hs5_2));
    printf("Average time spent to insert file2 in HashSet w/1e6 buckets: " + calcTimeHS(file2, hs6_2));
    
    //insert file3 into linkedlist and hashsets and calculate insertion time
    printf("Average time spent to insert file3 in linked list: " + calcTimeLL(file3, lp3));
    //insert file3 into hs w/1e3,4,5,6 buckets and calculate insertion time
    printf("Average time spent to insert file3 in HashSet w/1e3 buckets: " + calcTimeHS(file2, hs3_3));
    printf("Average time spent to insert file3 in HashSet w/1e4 buckets: " + calcTimeHS(file2, hs4_3));
    printf("Average time spent to insert file3 in HashSet w/1e5 buckets: " + calcTimeHS(file2, hs5_3));
    printf("Average time spent to insert file3 in HashSet w/1e6 buckets: " + calcTimeHS(file2, hs6_3));
    
    //insert file4 into linkedlist and hashsets and calculate insertion time
    printf("Average time spent to insert file4 in linked list: " + calcTimeLL(file4, lp4));
    //insert file4 into hs w/1e3,4,5,6 buckets and calculate insertion time
    printf("Average time spent to insert file4 in HashSet w/1e3 buckets: " + calcTimeHS(file4, hs3_4));
    printf("Average time spent to insert file4 in HashSet w/1e4 buckets: " + calcTimeHS(file4, hs4_4));
    printf("Average time spent to insert file4 in HashSet w/1e5 buckets: " + calcTimeHS(file4, hs5_4));
    printf("Average time spent to insert file4 in HashSet w/1e6 buckets: " + calcTimeHS(file4, hs6_4));
    
    
    
    
    //Average find time of file1 in linkedlist
    printf("Average find time spent to insert file1 in linked list: " + calcFindTimeLL(file1, lp1));
    //Averagefind time of file1 in hs w/1e3,4,5,6 buckets
    printf("Average find time spent to insert file1 in HashSet w/1e3 buckets: " + calcFindTimeHS(file1, hs3_1));
    printf("Average find time spent to insert file1 in HashSet w/1e4 buckets: " + calcFindTimeHS(file1, hs4_1));
    printf("Average find time spent to insert file1 in HashSet w/1e5 buckets: " + calcFindTimeHS(file1, hs5_1));
    printf("Average find time spent to insert file1 in HashSet w/1e6 buckets: " + calcFindTimeHS(file1, hs6_1));
    
    //Average find time of file2 in linkedlist
    printf("Average find time spent to insert file2 in linked list: " + calcFindTimeLL(file2, lp2));
    //Averagefind time of file2 in hs w/1e3,4,5,6 buckets
    printf("Average find time spent to insert file2 in HashSet w/1e3 buckets: " + calcFindTimeHS(file2, hs3_2));
    printf("Average find time spent to insert file2 in HashSet w/1e4 buckets: " + calcFindTimeHS(file2, hs4_2));
    printf("Average find time spent to insert file2 in HashSet w/1e5 buckets: " + calcFindTimeHS(file2, hs5_2));
    printf("Average find time spent to insert file2 in HashSet w/1e6 buckets: " + calcFindTimeHS(file2, hs6_2));
    
    //Average find time of file3 in linkedlist
    printf("Average find time spent to insert file3 in linked list: " + calcFindTimeLL(file3, lp3));
    //Averagefind time of file3 in hs w/1e3,4,5,6 buckets
    printf("Average find time spent to insert file3 in HashSet w/1e3 buckets: " + calcFindTimeHS(file3, hs3_3));
    printf("Average find time spent to insert file3 in HashSet w/1e4 buckets: " + calcFindTimeHS(file3, hs4_3));
    printf("Average find time spent to insert file3 in HashSet w/1e5 buckets: " + calcFindTimeHS(file3, hs5_3));
    printf("Average find time spent to insert file3 in HashSet w/1e6 buckets: " + calcFindTimeHS(file3, hs6_3));
    
    //Average find time of file4 in linkedlist
    printf("Average find time spent to insert file4 in linked list: " + calcFindTimeLL(file4, lp4));
    //Averagefind time of file4 in hs w/1e3,4,5,6 buckets
    printf("Average find time spent to insert file4 in HashSet w/1e3 buckets: " + calcFindTimeHS(file4, hs3_4));
    printf("Average find time spent to insert file4 in HashSet w/1e4 buckets: " + calcFindTimeHS(file4, hs4_4));
    printf("Average find time spent to insert file4 in HashSet w/1e5 buckets: " + calcFindTimeHS(file4, hs5_4));
    printf("Average find time spent to insert file4 in HashSet w/1e6 buckets: " + calcFindTimeHS(file4, hs6_4));
}