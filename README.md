# CSE 109 - Final Exam

**Before you start on this exam, read everything *thoroughly* top to bottom**

Spring 2020

## Ethics Contract

**FIRST**: Please read the following carefully:

-	I have not received, I have not given, nor will I give or receive, any assistance to another student taking this exam, including discussing the exam with students in another section of the course. Do not discuss the exam after you are finished until final grades are submitted.
-	I will not plagiarize someone else's work and turn it in as my own. If I use someone else's work in this exam, I will cite that work. Failure to cite work I used is plagiarism.
-	I understand that acts of academic dishonesty may be penalized to the full extent allowed by the Lehigh University Code of Conduct, including receiving a failing grade for the course. I recognize that I am responsible for understanding the provisions of the Lehigh University Code of Conduct as they relate to this academic exercise.

If you agree with the above, type your full name in the following space along with the date. Your exam **will not be graded** without this assent.

*** Writing your name and the date below binds you to the above agreement

Jingwen Wei

*** Writing your name and the date above binds you to the above agreement

## Prelude

First of all, we're going to do things the same way we did them in Exam 2. You must have at least one commit per question. For each question, subsequently add the hash of the first commit for that question in this README at the indicated space. I think pretty much everyone got this right on Exam 2, so just do the same thing here.

What you're going to do for this exam is benchmark your linked list and hash set implementations. This is the "science" part of computer science. You've already implemented some data structures, and now it's time to see how they perform relative to one another and compare that to what the theory states. Until now we've been using our data structures on very small datasets. I've included a large dataset of 1 million URLs in this repository. I've broken it up into files of varying size because the full 1 million can be hard to work with. Get your program working with the smaller dataset first, and then ramp up to the larger ones. You can complete this exam without getting up to the full 1 million, but I'd like you to get at least to 10^5 elements.

Just like Exam 2 there will be two parts. First you'll right some code, then you'll answer some questions. For part 1, you are going to be implementing a program in main.c that will read a file of URLs, and then insert the entires into a datastructure (either a linked list or a hash set, depeing on what you are measuring). You will time how long it takes to insert entires into the data structure, and then also how long it takes to find them.

For timing, I've got an example of measuring how much time elapses in a program working here: https://repl.it/repls/QuarterlyWelltodoChapters

You are free to use any other methods that you see fit, but this is just an example.

For reading files, you can use stdio: http://www.cplusplus.com/reference/cstdio/
Here is a source with some examples: https://www.tutorialspoint.com/cprogramming/c_file_io.htm

You can get your project started without reading files by copy/pasting string literals into the main.c source. But only do that for a couple strings. When you want to read in thousands of strings, you should switch to reading a file.

**Keep in mind** This is a very open ended exam. I do not care so much about the code you write. What I really care about are two things:

1. That you collect some data
2. That you analyze that data.

Questions 6 and 7 are basically "generate the data table" that you see below. It's up to you how you want to do this. You can write a single program that does it all automatically in one pass, or you can do it manually by changing your main.c code each time. Remember to commit your code though if you change it after each pass.

## Part 1

You will write a program that reads URLs from a file. You will use these URLs to build the following 5 data structures

1. 1e3/1e4/1e5/1e6 URLs – Linked List
2. 1e3/1e4/1e5/1e6 URLs – Hash Set with 1e3 Buckets
3. 1e3/1e4/1e5/1e6 URLs – Hash Set with 1e4 Buckets
4. 1e3/1e4/1e5/1e6 URLs – Hash Set with 1e5 Buckets
5. 1e3/1e4/1e5/1e6 URLs – Hash Set with 1e6 Buckets

You will generate some statistics for each data structure.

1. Record the average insertion time for each data structure.
2. Record the average find time for each data structure.

In the end you will have 5 data structures with two measurements each. You will make a data table that looks like this:

Table 1:
```
Average Insertion Time (ns)
n                  1e3     |      1e4      |       1e5     |      1e6      |
Linked List                |               |               |               |
Hash Set 1e3               |               |               |               |  
Hash Set 1e4               |               |               |               |
Hash Set 1e5               |               |               |               |
Hash Set 1e6               |               |               |               |

Average Find Time (ns)
n                  1e3     |      1e4      |       1e5     |      1e6      |
Linked List                |               |               |               |
Hash Set 1e3               |               |               |               |  
Hash Set 1e4               |               |               |               |
Hash Set 1e5               |               |               |               |
Hash Set 1e6               |               |               |               |
```

Where each cell will contain the average insert or find time in nanoseconds.

### Question 1

Lay out your directory. Create a src folder, a lib folder, a include folder, and a blank Makefile. 

- Inside the `src` folder create a `bin` directory. 
- Inside the `src/bin` directory create `main.c`
- Inside the `src` directory create `lib.c`. lib.c is for implementing any helper functions you want to use in `main.c`. The only function in `main.c` should be `main()`.
- Inside the `include` directory add `benchmarker.h`

Hash:2162603bdf88f624cb6af70b3541398fb83e2e6c

### Question 2

Build your linked list and hash set libraries from previous homeworks and place them in the lib directory, along with their respective header files.

Hash: fe380af48bd2314f201a68c82f50901992102a1f

### Question 3

Implement the Makefile to build your project.

- `make` will build `main.c`, linking to `liblinkedlist.a` and `libhashset.a`. All object files should be placed in build/objects and all binary executables should be placed in `build/bin`.
- `make lib` will make `libbenchmarker.a` and put it in `build/lib`.
- `make clean` will clean the project of all build artifacts (*.o main). It shouldn't get rid of the libraries in `lib`

Hash:d35e969f8bcecac549ab7696243ff6028629d656

### Question 4

In `main.c` have it read in a single argument from the command line. This argument will be the file to read in when you call main.c. For example you will call

```
./main urls100.txt
```

And your program will then read in urls100.txt line by line. Don't do the file reading yet though, that's the next question. Just read the argument in this question.

Hash:cd804fb52982ceee572d83fb73200f5c979574d8

### Question 5

Read in the supplied file.

Hash:89594e909379d5dd2a7f9b9ccc48ded377503162

### Question 6

Write code to generate data for average insertion time for each of the 5 data structures. For each data structure, you'll create an empty data structure, then insert N items into it, and time how long that takes, then divide the result by the number of elements inserted.

For example, if it takes 1000 nanoseconds to insert 100 items into a linked list, that's an average of 10 ns per item.

Hash:1e15863bd337653f78e647cb0da44e39688db4c8

### Question 7

Write code to generate data for average find time for each of the 5 data structures. To do this, you'll first have to populate an empty data structure with some items (Question 6). Then, time how long it takes to find each item in that data structure and get the average.

For example, if I insert 100 items into a linked list, then I can run a loop and find those 100 items in the linked list. Time how long that takes and divide by 100.

Hash:d2d2bdcad4a5453ec452d8aa53d1966290c0b826

## Part 2

### Question 8

Describe your method for generating the data in Table 1. 

First I create 20 data structure. lp1-4, every linked list for one file. HashSets are labeled as follow, hsB_F, where B is the bucket size, and F is the file it corresponds to.
Then by calling calcTimeLL and calcTimeHS, each of the method start calculating the start time when the method starts inserting the first element into the data structure, and stops the time when 
everything is successfully entered. So the output of these two method is the time spent for inserting the elemenets into every data structure. 
Also for calculating the find time, I used the method calcFindTimeLL and calcFindTimeHS. The method starts timing when searching for the first element in the file, and stops timing when it search 
through every elements in the file. In the main, I output the time spent for all the above steps into the console. 

Hash: a76a84f5ce429cf28a964fedb0350fd0601df78f

### Question 9

Describe what if any trends you see in the data.

linked list is the slowest when it comes to inserting and finding datas. As the number of items increase (especially with 1e5, and 1e6),
it takes a great amount of time to run the searching and inserting of linked list, and my computer couldn't handle that much. As the bucket size of 
the hash set gets greater, it is faster to insert and search in the hash set. 

Hash:ca9e402b8e24048cdde3e26872a1145d53b9c38d

### Question 10

What does the theory say the results should be? Do your findings match your expectation? What factors could have influenced your results?

My findings match the theory because just like the theory says, as the bucket size of the hashset increase, the insert and find time is faster because the table is split into several differents parts. Several linkedlist is easier to track
than fewer oenes. Factors that could have influenced my results are things like way I code for insertion and find elemnts, the speed of my computers running the methods... 

Hash: 82f556ad25b2e7b9cbd8802ffa28670190b5aba1

### Question 11

Include two graphs that summarize your results
  - One graph that depicts n versus average insertion time for each of the 5 data structures
  - One graph that depicts n versus average search time for each of the 5 data structures
  - I suggest a log scale on the Y axis for average find time.
  - Each graph will have 5 data series with 4 data points each for a total of 20 data points per graph
  - Be sure to embed both graphs in your report using Markdown.
  - You can draw the graphs using any program you are familiar with (I suggest Excel or its equivalent on other platforms). Export your graphs from these programs in a common format though (jpg, png, or pdf).
  - Also include the underlying data (e.g. the Excel file you used to generate your graphs)
  - I've included example graphs here to give you an idea of what they look like. Don't worry if yours don't not look like this. There are a million factors that play into the results you ultimately get. Your job is to get the data, and then provide an analysis of the data. There are no "right" data. If your data does not match the theory, explain why. If it does match, explain how.

I include the graph in the gitlab files

Hash: 9a7109e88371ec1935c70c7ac41a4586b7eb95bf

## Postscript

Alright, that's it. Have this in by Monday at 5pm, and then have a great summer!