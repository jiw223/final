all: main
    mkdir -p build/bin
    mkdir -p build/lib
    mkdir -p build/objects
    mv *.o build/objects
    mv *.a build/lib
    mv main build/bin
    
main.o: src/bin/main.c
    gcc src/bin/main.c -c -I inlcude -I lib
    
main: main.o libhashset.a liblinkedlist.a libbenchmarker.a
    gcc -g main.o -o main -lhashset -llinkedset -lbenchmarker -L.
    
libhashset.a: src/lib.c
    gcc src/lib.c -c -I include -I lib
    ar rs libhashset.a lib.o
    
liblinkedlist.a: src/lib.c
    gcc src/lib.c -c -I include -I
    ar rs liblinkedlist.a lib.o
    
lib libbenchmarker.a: src/lib.c
    gcc src/lib.c -c -I include
    ar rs libbenchmarker.a lib.o
    
clean:
    rm -rf build